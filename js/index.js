 $(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
          interval: 2000
        });

        $('#newsletter').on('show.bs.modal', function(e){
          console.log('el modal newsletter se esta mostrando');

          $('#newsletterBtn').removeClass('btn-primary'); $('#newsletterBtn').addClass('btn-outline-secondary'); 
          $('#newsletterBtn').prop('disabled', true);

        });

        $('#newsletter').on('shown.bs.modal', function(e){
          console.log('el modal newsletter se mostró');
        });

        $('#newsletter').on ('hide.bs.modal', function(e){ 
          console.log('el modal contacto se esta ocultando'); 
        });

        $('#newsletter').on ('hidden.bs.modal', function(e){ 
          console.log('el modal contacto se ocultó'); 
          $('#newsletterBtn').removeClass('btn-outline-secondary'); $('#newsletterBtn').addClass('btn-primary');
          $('#newsletterBtn').prop('disabled', false);

        });

      });